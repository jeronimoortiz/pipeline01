locals {
    ec2_instances = {
        "AWSNVSSPRAP01P"  = { Name_ec2 = "AWSNVSSPRAP01P", instance_family = "t2.micro", ip = "", ami = "placeholder"}
    }
}

resource "aws_instance" "ec2" {
  for_each      = local.ec2_instances
  ami           = each.value.ami
  instance_type = each.value.instance_family
  # SSH KEYPAIR
  key_name      = "placeholder"
  disable_api_termination = true
  user_data = each.value.so == "wintel" ? file("win-user-data.txt") : ""
    network_interface {
    network_interface_id = aws_network_interface.ec2[each.key].id
    device_index = 0
  }
  root_block_device {
    volume_size           = "60"
    volume_type           = "gp3"
    encrypted             = true
    delete_on_termination = true
    kms_key_id            = var.kms_key_arn_ebs
  }

  tags = {Name = each.value.Name_ec2, so = each.value.so}
  volume_tags = merge({Name = each.value.Name_ec2, so = each.value.so}, local.tags)
  
  #IMPORTANTE!! si el modulo data encuentra una AMI mas nueva y lifecycle está comentado, se reemplazará la instancia!!
  lifecycle {
    ignore_changes = [
      ami,
      user_data]
  }

  #IMDSv2
  metadata_options {
    http_tokens = "required"
    http_put_response_hop_limit = "1"
  }

  #  only for T family instances
  # credit_specification {
  #   cpu_credits = "standard"
  # }

}

resource "aws_ebs_volume" "ec2" {
  for_each          = local.ec2_instances
  availability_zone = aws_instance.ec2[each.key].availability_zone
  size              = 100
  encrypted         = true
  kms_key_id        = var.kms_key_arn_ebs
  type              = "gp3"
}

resource "aws_volume_attachment" "ec2" {
  for_each      = local.ec2_instances
  device_name = each.value.so == "wintel" ? "xvdf" : "/dev/sdf"
  instance_id   = aws_instance.ec2[each.key].id
  volume_id     = aws_ebs_volume.ec2[each.key].id
  depends_on = [
    aws_instance.ec2
  ]
}

resource "aws_network_interface" "ec2" {
  for_each        = local.ec2_instances
  subnet_id       = var.subnet_ec2
  #descomentar si se usan fixed IPs
  #private_ips     = [each.value.ip]
  security_groups = var.sg_ec2
  tags            = {Name = each.value.Name_ec2, so = each.value.so}
  #En caso que la instancia haya tenido una modificación manual desde consola,
  # lifecycle previene que el codigo modifique los cambios manuales.
  lifecycle {
    ignore_changes = [security_groups]
  }

}

